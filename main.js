const sqlite3 = require('sqlite3').verbose();
const uuidv1 = require('uuid/v1');
const _ = require('lodash');
// const Promise = require('bluebird');
let db = new sqlite3.Database(':memory:');
let redisClient;

// db = Promise.promisifyAll(db);

db.getAsync = function (sql) {
  var that = this;
  return new Promise(function (resolve, reject) {
      that.get(sql, function (err, row) {
          if (err)
              reject(err);
          else
              resolve(row);
      });
  });
};

db.runAsync = function (sql) {
  var that = this;
  return new Promise(function (resolve, reject) {
      that.run(sql, function(err) {
          if (err)
              reject(err);
          else
              resolve();
      });
  });
};

const argv = process.argv.slice(2);
const doRedisTest = !_.isEmpty(argv) && argv[0] == 'redis';
if (doRedisTest) {
  redisClient = require('redis').createClient();

  redisClient.getAsync = function () {
    var that = this;
    const args = arguments;
    return new Promise(function (resolve, reject) {
      that.get(...args, function(err, ret) {
        if (err)
          reject(err);
        else
          resolve(ret);
      });
    });
  };

  redisClient.setAsync = function () {
    var that = this;
    const args = arguments;
    return new Promise(function (resolve, reject) {
      that.set(...args, function(err, ret) {
        if (err)
          reject(err);
        else
          resolve(ret);
      });
    });
  };
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

const randomKeys = _.map(Array(1000), (t) => {
  return getRandomIntInclusive(1, 10000);
});

(async function() {
  await db.runAsync(`create table player(
    id integer primary key,
    uuid text not null
  );`);
  const json = {};
  for (let i = 1; i <= 10000; ++i) {
    const uuid = uuidv1();
    json[i] = uuid;
    await db.runAsync(`insert into player(id, uuid) values(${i}, '${uuid}');`);
    if (doRedisTest) {
      await redisClient.setAsync(i, uuid);
    }
  }
  // await Promise.all(Array(10000).map(function(_val) {
  //   const uuid = uuidv1();
  //   json[i] = uuid;
  //   return await db.runAsync(`insert into player(id, uuid) values(${i}, '${uuid}');`);
  // }));

  console.log(typeof json);

  console.time('sqlite with template literals');
  await Promise.all(randomKeys.map(async (k) => {
    return await db.getAsync(`select * from player where id=${k}`);
  }));
  console.timeEnd('sqlite with template literals');

  console.time('sqlite');
  await Promise.all(randomKeys.map(async (k) => {
    return await db.getAsync('select * from player where id=?', k);
  }));
  console.timeEnd('sqlite');

  if (doRedisTest) {
    console.time('redis');
    await Promise.all(randomKeys.map(async (k) => {
      return await redisClient.getAsync(k);
    }));
    console.timeEnd('redis');
  }

  let tmp;
  console.time('json');
  _.forEach(randomKeys, (k) => {
    tmp = json[k];
  });
  console.timeEnd('json');

  console.time('json with simple condition');
  _.forEach(randomKeys, (k) => {
    if (!_.has(json, k)) {
      console.log(`failed to find ${k}`);
    }
  });
  console.timeEnd('json with simple condition');

  await db.runAsync(`drop table player;`);
  await db.close();
  if (doRedisTest) {
    redisClient.flushdb();
    redisClient.quit();
  }
})();